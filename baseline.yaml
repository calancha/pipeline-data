---
# Baseline testing from git trees
.report_rule_to_failed_test_maintainers: &report_rule_to_failed_test_maintainers
  when: failed_tests
  send_to: failed_tests_maintainers

.default:
  architectures: 'x86_64 ppc64le aarch64 s390x'
  cki_pipeline_project: cki-trusted-contributors
  kernel_type: upstream  # needs to match cki_pipeline_project!
  kpet_tree_family: rawhide
  kpet_tree_name: rawhide
  builder_image: quay.io/cki/builder-rawhide
  config_target: 'olddefconfig'
  make_target: 'targz-pkg'
  test_set: 'kt1'
  scratch: 'false'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]

.with_selftests:
  # debug kernels are too large for gitlab artifacts
  test_debug: 'true'
  build_selftests: 'true'
  test_set: 'kt1|kself'

.ark-clang-base:
  git_url: https://gitlab.com/cki-project/kernel-ark.git
  .branches:
    - ark-latest
  cki_pipeline_branch: ark
  make_target: rpm
  srpm_make_target: dist-srpm
  compiler: clang

ark-rawhide-clang:
  .extends: .ark-clang-base
  rpmbuild_with: up
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: 'tstellar@redhat.com'}

ark-rawhide-clang-lto:
  .extends: .ark-clang-base
  rpmbuild_with: 'up clang_lto'
  architectures: 'x86_64 aarch64'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - when: always
      send_to:
        - tstellar@redhat.com
        - tbaeder@redhat.com

ark-eln-clang:
  .extends: .ark-clang-base
  builder_image: quay.io/cki/builder-eln
  native_tools: 'true'
  kpet_tree_family: eln
  kpet_tree_name: eln
  # ELN tags are moving forward and our container tag is already behind koji.
  # According to dzickus forcing the "eln" override may be better than having
  # a tag that's too behind due to potential package/kernel installation issues
  # or dependencies.
  disttag_override: '.eln'
  rpmbuild_with: up
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: 'tstellar@redhat.com'}

.mainline-base:
  .extends: .with_selftests
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
  .branches:
    - master
  cki_pipeline_branch: mainline.kernel.org

mainline.kernel.org:
  .extends: .mainline-base
  domains: 'general|dedicated ampere_altra'
  selftest_subsets: 'net bpf'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: 'nathan@kernel.org'}

mainline.kernel.org-clang:
  .extends: .mainline-base
  compiler: clang
  selftest_subsets: 'net bpf seccomp lkdtm'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: 'llvm@lists.linux.dev'}

net-next:
  .extends: .with_selftests
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/davem/net-next.git
  .branches:
    - master
  cki_pipeline_branch: net-next
  test_set: 'net'
  selftest_subsets: net

scsi:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/mkp/scsi.git
  .branches:
    - for-next
  cki_pipeline_branch: scsi
  test_set: 'stor'

arm-next:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git
  .branches:
    - for-kernelci
  cki_pipeline_branch: arm
  architectures: 'aarch64'
  domains: 'general|dedicated ampere_altra'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - when: always
      send_to:
        - will@kernel.org
        - catalin.marinas@arm.com
        - linux-arm-kernel@lists.infradead.org

arm-acpi:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/lpieralisi/linux.git
  .branches:
    - acpi/for-next
  cki_pipeline_branch: arm
  architectures: 'aarch64'
  test_set: 'acpi'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: lorenzo.pieralisi@arm.com}

stable:
  git_url:
    https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
  .branches:
    - linux-6.3.y
  cki_pipeline_branch: upstream-stable
  .report_rules:
    # don't notify test maintainers about failures
    # https://gitlab.com/cki-project/pipeline-data/-/issues/11
    # - !reference [.report_rule_to_failed_test_maintainers]
    # Do not send emails to stable until we discuss requirements with the
    # tree maintainers!
    # - {when: always, send_to: stable@vger.kernel.org}
    - {when: always, send_to: jforbes@redhat.com}

stable-queue:
  git_url:
    https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
  .branches:
    - queue/6.3
  cki_pipeline_branch: upstream-stable
  .report_rules:
    # don't notify test maintainers about failures
    # https://gitlab.com/cki-project/pipeline-data/-/issues/11
    # - !reference [.report_rule_to_failed_test_maintainers]
    # Do not send emails to stable until we discuss requirements with the
    # tree maintainers!
    # - {when: always, send_to: stable@vger.kernel.org}
    - {when: always, send_to: jforbes@redhat.com}

.rt-base:
  .extends: .with_selftests
  architectures: 'x86_64'
  rt_kernel: 'True'
  selftest_subsets: 'net livepatch bpf'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: kernel-rt-ci@redhat.com}

rt-devel:
  .extends: .rt-base
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git
  .branches:
    - linux-6.3.y-rt
  cki_pipeline_branch: rt-devel

rt-stable:
  .extends: .rt-base
  git_url:
    https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-stable-rt.git
  .branches:
    - v6.1-rt
    - v5.15-rt
    - v5.10-rt
    - v4.19-rt
  cki_pipeline_branch: rt-stable

block:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/axboe/linux-block.git
  .branches:
    - for-next
    - for-current
  cki_pipeline_branch: block
  test_set: 'kt0|stor|fs'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    # Do not send emails to stable until we discuss requirements with the
    # tree maintainers!
    # - when: failed
    #   send_to:
    #     - linux-block@vger.kernel.org
    #     - axboe@kernel.dk

bpf-next:
  .extends: .with_selftests
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/bpf/bpf-next.git
  .branches:
    - master
  cki_pipeline_branch: bpf
  architectures: 'x86_64'
  builder_image: quay.io/cki/builder-rawhide-llvm
  test_set: 'bpf'
  selftest_subsets: 'bpf net'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]

kvm:
  .extends: .with_selftests
  git_url: https://git.kernel.org/pub/scm/virt/kvm/kvm.git
  .branches:
    - master
    - next
  cki_pipeline_branch: kvm
  architectures: 'x86_64'
  test_set: 'virt'
  selftest_subsets: 'kvm'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: mcondotta@redhat.com}

gfs2:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/gfs2/linux-gfs2.git
  .branches:
    - for-next
  cki_pipeline_branch: gfs2
  test_set: 'gfs2'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: gfs2-maint@redhat.com}

intel-lts:
  git_url: https://github.com/intel/linux-intel-lts.git
  .branches:
    - rebasing/5.15-linux
  cki_pipeline_branch: intel-lts
  architectures: 'x86_64'

.net-queue-base:
  cki_pipeline_branch: net-queue
  architectures: 'x86_64'
  .branches:
    - dev-queue
  test_set: 'net-ice'
  .report_rules:
    - when: always
      send_to:
        - kzhang@redhat.com
        - dkc@redhat.com
        - ctrautma@redhat.com
        - zfang@redhat.com
        - jtoppins@redhat.com

tnguy-net:
  .extends: .net-queue-base
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/tnguy/net-queue.git

tnguy-next:
  .extends: .net-queue-base
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/tnguy/next-queue.git
